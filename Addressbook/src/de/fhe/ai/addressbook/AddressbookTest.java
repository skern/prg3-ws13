package de.fhe.ai.addressbook;

import de.fhe.ai.addressbook.storage.IStorageController;
import de.fhe.ai.addressbook.storage.JsonStorageController;
import de.fhe.ai.addressbook.storage.ObjectStorageController;
import de.fhe.ai.addressbook.model.Address;
import de.fhe.ai.addressbook.model.Addressbook;
import de.fhe.ai.addressbook.model.Person;
import de.fhe.ai.addressbook.model.SortCriteriaEnum;

import java.io.FileNotFoundException;

/**
 * Created with IntelliJ IDEA.
 * User: steffenkern
 * Date: 07.11.13
 * Time: 16:42
 * To change this template use File | Settings | File Templates.
 */
public class AddressbookTest
{
    public static void main( String[] args )
    {

        /*
            Create a new addressbook and add some data
         */
        Addressbook ab = new Addressbook();

        Address address01 = new Address( "Altonaer Strasse 25",     "Erfurt", "99085" );
        Address address02 = new Address( "Leipziger Strasse 77",    "Erfurt", "99085" );
        Address address03 = new Address( "Schlueterstrasse 1",       "Erfurt", "99089" );
        Address address04 = new Address( "Marktstrasse 6",          "Weimar", "99423" );

        ab.addPerson( new Person( "Max",    "Mustermann",   "m.m@m.com",        address01 ) );
        ab.addPerson( new Person( "Michal", "Mustermann",   "m.muster@m.com",   address02 ) );
        ab.addPerson( new Person( "Martin", "Schmidt",      "m.s@m.com",        address03 ) );
        ab.addPerson( new Person( "Volker", "Schulz",       "v.s@m.com",        address04 ) );
        ab.addPerson( new Person( "Hans",   "Meier",        "hans.m@m.com",     address01 ) );
        ab.addPerson( new Person( "Gabi",   "Mustermann",   "g.m@m.com",        address03 ) );
        ab.addPerson( new Person( "Heike",  "Meier",        "h.m@m.com",        address04 ) );

        /*
            Test sort methods
         */

        System.out.println( "Persons sorted by lastname descending *****" );
        System.out.println( ab.sortPersonList( SortCriteriaEnum.LASTNAME_DESC ) + "\n" );

        System.out.println( "Persons sorted by mail ascending **********" );
        System.out.println( ab.sortPersonList( SortCriteriaEnum.MAIL_ASC) + "\n" );

        System.out.println( "Persons sorted by street ascending ********" );
        System.out.println( ab.sortPersonListWithEnhancedEnum( SortCriteriaEnum.STREET_ASC) + "\n" );


        /*
            Save & Load using Object Output Stream
         */

        IStorageController sc = new ObjectStorageController();
        sc.saveAddressbook( ab );

        try
        {
            ab = sc.loadAddressbook();
        }
        catch ( FileNotFoundException fnfe )
        {
            System.out.println( "Data File not available" );
            fnfe.printStackTrace();
        }

        /*
            Save & Load using Json Mapper
         */

        sc = new JsonStorageController();
        sc.saveAddressbook( ab );

        try
        {
            ab = sc.loadAddressbook();

            ab.printCityOccurence();
        }
        catch ( FileNotFoundException fnfe )
        {
            System.out.println( "JSON File not available" );
            fnfe.printStackTrace();
        }


    }
}
