package de.fhe.ai.addressbook;

import de.fhe.ai.addressbook.model.Address;
import de.fhe.ai.addressbook.model.Addressbook;
import de.fhe.ai.addressbook.model.Person;

import java.util.Scanner;

/**
 * Simple Test Class showing how to handle input from terminal
 * and fill an Addressbook with entries.
 *
 * @see Addressbook
 */
public class InteractiveAddressbookTest
{
    private static final Addressbook ab = new Addressbook();

    public static void main( String[] args )
    {
        showMenu();
    }

    private static void showMenu()
    {
        Scanner cmdScanner = new Scanner( System.in );
        int command;
        boolean run = true;

        while( run )
        {
            System.out.println( "Bitte wählen Sie eine Option:" );
            System.out.println( "1) Neue Person hinzufügen" );
            System.out.println( "2) Alle Personen ausgeben" );
            System.out.println( "\n0) Programm beenden" );

            command = cmdScanner.nextInt();
            cmdScanner.nextLine();

            switch( command )
            {
                case 1:
                    addPersonFromTerminal( cmdScanner, ab );
                    break;

                case 2:
                    System.out.println( ab.getPersons() );
                    break;

                default:
                    run = false;
            }
        }
    }

    private static void addPersonFromTerminal( Scanner cmdScanner, Addressbook ab )
    {
        Person p = new Person();
        p.setAddress( new Address() );

        p.setLastName( getValue( cmdScanner, "Name:" ) );
        p.setFirstName( getValue( cmdScanner, "Vorname:" ) );
        p.setMail( getValue( cmdScanner, "Email:" ) );

        p.getAddress().setStreet( getValue( cmdScanner, "Strasse:" ) );
        p.getAddress().setCity( getValue( cmdScanner, "Wohnort:" ) );
        p.getAddress().setZipCode( getValue( cmdScanner, "Postleitzahl:" ) );

        ab.addPerson( p );
    }

    private static String getValue( Scanner cmdScanner, String prompt )
    {
        System.out.println( prompt );
        return cmdScanner.nextLine().trim();
    }

}
