package de.fhe.ai.addressbook.control;

import java.io.IOException;

/**
 * "Global" Class for Exception Handling purposes
 * Defines several static, overloaded methods for
 * various Exception types.
 */
public class ExceptionHandler
{
    /**
     * Method to handle <code>IOExceptions</code>
     *
     * @param ioe the exception which should be handled
     * @param source the class that catched the exception
     * @param message an optional message to describe the exception
     */
    public static void handle( IOException ioe, Object source, String message )
    {
        System.out.println( "Error during I/O operation" );
        System.out.println( "Error occurred in " + source.getClass() );

        if( message != null ) System.out.println( "Message: " + message );

        ioe.printStackTrace();
    }

    /**
     * Method to handle <code>IOExceptions</code>
     *
     * @param ioe the exception which should be handled
     * @param source the class that catched the exception
     */
    public static void handle( IOException ioe, Object source )
    {
        handle( ioe, source, null );
    }

    /**
     * Method to handle <code>ClassNotFoundException</code>
     *
     * @param cnfe the exception which should be handled
     * @param source the class that catched the exception
     * @param message an optional message to describe the exception
     */
    public static void handle( ClassNotFoundException cnfe, Object source, String message )
    {
        System.out.println( "Class could not be loaded" );
        System.out.println( "Error occurred in " + source.getClass() );

        if( message != null ) System.out.println( "Message: " + message );

        cnfe.printStackTrace();
    }

    /**
     * Method to handle <code>ClassNotFoundException</code>
     *
     * @param cnfe the exception which should be handled
     * @param source the class that catched the exception
     */
    public static void handle( ClassNotFoundException cnfe, Object source )
    {
        handle( cnfe, source, null );
    }
}
