package de.fhe.ai.addressbook.logic;

import de.fhe.ai.addressbook.model.Person;

import java.util.Comparator;

/**
 * Comparator compatible class which implements an ordering for
 * our Person class based on mail addresses
 */
public class AscendingEMailComparator implements Comparator<Person>
{
    @Override
    public int compare( Person person1, Person person2 )
    {
        if( person1.getMail() == null ) return 1;
        if( person2.getMail() == null ) return -1;

        return person1.getMail().compareTo( person2.getMail() );
    }
}
