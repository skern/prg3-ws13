package de.fhe.ai.addressbook.logic;

import de.fhe.ai.addressbook.model.Person;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: kern
 * Date: 19.11.13
 * Time: 11:24
 */
public class AscendingStreetComparator implements Comparator<Person>
{

    @Override
    public int compare( Person person1, Person person2 )
    {
        // Check if both persons have an address
        // If a person does not have an address, it is considered "larger", so all
        // Person instances without proper information will be at the end of a sorted list
        if( person1.getAddress() == null ) return 1;
        if( person2.getAddress() == null ) return -1;

        // Check if both persons have a street entry in their address objects
        // Persons without a proper street are again considered "larger"
        if( person1.getAddress().getStreet() == null ) return 1;
        if( person2.getAddress().getStreet() == null ) return -1;

        // Ok, both persons have an address with a valid street instance
        // Let's check if streets are the same - if so, we compare both persons
        // using their compareTo method
        if( person1.getAddress().getStreet().equals( person2.getAddress().getStreet() ) )
        {
            return person1.compareTo( person2 );
        }
        // Otherwise, we compare both street strings
        else
        {
            return person1.getAddress().getStreet().compareTo( person2.getAddress().getStreet() );
        }
    }
}
