package de.fhe.ai.addressbook.logic;

import de.fhe.ai.addressbook.model.Person;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: kern
 * Date: 19.11.13
 * Time: 11:10
 */
public class DescendingEMailComparator implements Comparator<Person>
{
    private AscendingEMailComparator ascendingEMailComparator = new AscendingEMailComparator();

    @Override
    public int compare( Person person1, Person person2 )
    {
        return -1 * ascendingEMailComparator.compare( person1, person2 );
    }
}
