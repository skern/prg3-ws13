package de.fhe.ai.addressbook.logic;

import de.fhe.ai.addressbook.model.Person;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: kern
 * Date: 19.11.13
 * Time: 11:12
 */
public class DescendingLastnameComparator implements Comparator<Person>
{
    @Override
    public int compare( Person person1, Person person2 )
    {
        return -1 * person1.compareTo( person2 );
    }
}
