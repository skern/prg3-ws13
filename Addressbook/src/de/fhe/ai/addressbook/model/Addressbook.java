package de.fhe.ai.addressbook.model;

import de.fhe.ai.addressbook.logic.AscendingEMailComparator;
import de.fhe.ai.addressbook.logic.AscendingStreetComparator;
import de.fhe.ai.addressbook.logic.DescendingEMailComparator;
import de.fhe.ai.addressbook.logic.DescendingLastnameComparator;
import de.fhe.ai.addressbook.model.Person;
import de.fhe.ai.addressbook.model.SortCriteriaEnum;

import java.io.Serializable;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: steffenkern
 * Date: 07.11.13
 * Time: 16:41
 * To change this template use File | Settings | File Templates.
 */
public class Addressbook implements Serializable
{
    private static final long serialVersionUID = -4224741350801502250L;

    private List<Person> persons = new ArrayList<Person>();

    // Private Dictionary to map lastnames to a list of persons with this lastname
    private Map<String, List<Person>> personDict = new HashMap<String, List<Person>>();


    /**
     * The default constructor, which creates an empty address book.
     */
    public Addressbook() {}


    /**
     * This method is used to add a new person to the address book.
     *
     * Beside adding this person to our internal person list, we also add this person to our
     * helper map which maps lastnames to persons. We do this to achieve a better running time
     * during the execution of our <code>printLastnameOccurence</code> method.
     *
     * @param p a Person object that should be added to our address book
     */
    public void addPerson( Person p )
    {
        this.persons.add( p );

        /*
            Add Person to our Lastname Dictionary
         */
        List<Person> lastnameList = this.personDict.get( p.getLastName() );

        // If this is a new lastname - create a new entry
        if( lastnameList == null )
        {
            lastnameList = new ArrayList<Person>( 1 );
            this.personDict.put( p.getLastName(), lastnameList );
        }

        lastnameList.add( p );
    }

    /**
     * Returns an (unsorted) list of persons in the address book.
     *
     * @return a list of all persons without any specific order
     */
    public List<Person> getPersons()
    {
        return this.persons;
    }

    /**
     * This method returns a sorted list of persons - the sort order can be selected using
     * the <code>sortCritera</code> parameter.
     * During execution, a new list is created - the original one is not changed.
     *
     * @param sortCriteria the sort order for the returned list
     * @return a sorted list of all persons in the address book
     */
    public List<Person> sortPersonList( SortCriteriaEnum sortCriteria )
    {
        List<Person> sortedList = new ArrayList<Person>( this.getPersons() );

        switch( sortCriteria )
        {
            case LASTNAME_DESC:
                Collections.sort( sortedList, new DescendingLastnameComparator() );
                break;
            case MAIL_ASC:
                Collections.sort( sortedList, new AscendingEMailComparator() );
                break;
            case MAIL_DESC:
                Collections.sort( sortedList, new DescendingEMailComparator() );
                break;
            case STREET_ASC:
                Collections.sort( sortedList, new AscendingStreetComparator() );
                break;
            default:    // Default used the compareTo Method of class Person, which sorts lastnames ascending
                Collections.sort( sortedList );
        }

        return sortedList;
    }

    /**
     * Simpler version of our sort method which used the extended SortCriteriaEnum
     *
     * @param sortCriteria Enum value that describes the requested sorting order and carries with it an instance of
     *                     the corresponding Comparator
     * @return a sorted list of all persons in the address book
     */
    public List<Person> sortPersonListWithEnhancedEnum( SortCriteriaEnum sortCriteria )
    {
        List<Person> sortedList = new ArrayList<Person>( this.getPersons() );

        if( sortCriteria.hasComparator() )
            Collections.sort( sortedList, sortCriteria.getComparator() );
        else
            Collections.sort( sortedList );

        return sortedList;
    }


    /**
     * Alternate option to create the map of all lastnames and the corresponding
     * person lists.
     *
     * Can be used, if one does not want to maintain a map during every person insert
     * in our <code>addPerson</code> method.
     *
     * @return a map that links lastnames to a list of persons with this lastname
     */
    private Map<String, List<Person>> prepareLastnameMap()
    {
        Map<String, List<Person>> map = new HashMap<String, List<Person>>(  );

        for ( Person p : this.getPersons() )
        {
            String lastname = p.getLastName();

            List<Person> lastnameList = map.get( lastname );

            if( lastnameList == null )
            {
                lastnameList = new ArrayList<Person>();

                map.put( lastname, lastnameList );
            }

            lastnameList.add( p );
        }

        return map;
    }

    /**
     * A helper method that creates a map which relates cities to the persons which live
     * in those cities.
     *
     * @return a map that relates cities to the persons that live in those cities
     */
    private Map<String, List<Person>> prepareCityMap()
    {
        final String unknownCity = "Unbekannte Stadt";

        Map<String, List<Person>> map = new HashMap<String, List<Person>>(  );

        for ( Person p : this.getPersons() )
        {
            String city = null;

            if( p.getAddress() == null ) city = unknownCity;
            else if( p.getAddress().getStreet() == null ) city = unknownCity;
            else city = p.getAddress().getCity();

            List<Person> cityList = map.get( city );

            if( cityList == null )
            {
                cityList = new ArrayList<Person>();
                map.put( city, cityList );
            }

            cityList.add( p );
        }

        return map;
    }

    /**
     * Prints a list of lastnames contained in the address book as well
     * as the number of persons that have the corresponding lastname.
     */
    public void printLastnameOccurence()
    {
        for( String lastname : this.personDict.keySet() )
        {
            System.out.print( lastname );
            System.out.print( ": \t" );
            System.out.println( this.personDict.get( lastname ).size() );
        }
    }

    /**
     * Prints a list of cities as well as the list of person from that cities
     */
    public void printCityOccurence()
    {
        Map<String, List<Person>> cityMap = this.prepareCityMap();

        for( String city : cityMap.keySet() )
        {
            System.out.print( city );
            System.out.print( ": \t" );
            System.out.println( cityMap.get( city ).size() );
        }
    }
}
