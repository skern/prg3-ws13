package de.fhe.ai.addressbook.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: steffenkern
 * Date: 07.11.13
 * Time: 16:39
 * To change this template use File | Settings | File Templates.
 */
public class Person implements Comparable<Person>, Serializable
{
    private static final long serialVersionUID = -8246661231196352702L;

    private String firstName;
    private String lastName;
    private String mail;

    private Address address;

    /*
        Constructors
     */

    /**
     * Default constructor that creates an empty person
     */
    public Person() {}

    /**
     * Prefered constructor which initializes a new Person instance with the given
     * firstname, lastname and email address.
     *
     * @param firstName
     * @param lastName
     * @param mail
     */
    public Person( String firstName, String lastName, String mail )
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
    }

    public Person( String firstName, String lastName, String mail, Address address )
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.address = address;
    }

    /**
     * Compares to Person instances. Overridden from Comparable<Person>
     *
     * @param o another person to compare to
     *
     * @return  the sort order of the two person objects (-1, 0, or 1)
     */
    @Override
    public int compareTo( Person o )
    {
        if( this.getLastName().equalsIgnoreCase( o.getLastName() ) )
        {
            return this.getFirstName().compareTo( o.getFirstName() );
        }
        else
        {
            return this.getLastName().compareTo( o.getLastName() );
        }
    }

    @Override
    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        if ( o == null || getClass() != o.getClass() )
        {
            return false;
        }

        Person person = (Person) o;

        if ( !firstName.equals( person.firstName ) )
        {
            return false;
        }
        if ( !lastName.equals( person.lastName ) )
        {
            return false;
        }
        if ( !mail.equals( person.mail ) )
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + mail.hashCode();
        return result;
    }

    /**
     * Overridden from <code>Object</code>
     *
     * Returns a human readable representation of a Person object.
     *
     * @return a String describing the person.
     */
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder( "Person{" );
        sb.append( "firstName='" ).append( firstName ).append( "', " );
        sb.append( "lastName='"  ).append( lastName  ).append( "', " );
        sb.append( "mail='"      ).append( mail      ).append( "', " );

        if( address != null && address.getStreet() != null  )
            sb.append( "street='" ).append( address.getStreet() ).append( "'" );

        sb.append( "}" );

        return sb.toString();
    }

    /*
        Getter & Setter
     */

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName( String firstName )
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName( String lastName )
    {
        this.lastName = lastName;
    }

    public String getMail()
    {
        return mail;
    }

    public void setMail( String mail )
    {
        this.mail = mail;
    }

    public Address getAddress()
    {
        return address;
    }

    public void setAddress( Address address )
    {
        this.address = address;
    }
}
