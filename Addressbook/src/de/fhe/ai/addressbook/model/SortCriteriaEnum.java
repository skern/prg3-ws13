package de.fhe.ai.addressbook.model;

import de.fhe.ai.addressbook.logic.AscendingEMailComparator;
import de.fhe.ai.addressbook.logic.AscendingStreetComparator;
import de.fhe.ai.addressbook.logic.DescendingLastnameComparator;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: kern
 * Date: 19.11.13
 * Time: 11:14
 */
public enum SortCriteriaEnum
{
    LASTNAME_ASC( null ),
    LASTNAME_DESC( new DescendingLastnameComparator() ),
    MAIL_ASC( new AscendingEMailComparator() ),
    MAIL_DESC( new DescendingLastnameComparator() ),
    STREET_ASC( new AscendingStreetComparator() );

    private Comparator<? super Person> comparator;

    private SortCriteriaEnum( Comparator<? super Person> comparator )
    {
        this.comparator = comparator;
    }

    public Comparator<? super Person> getComparator()
    {
        return this.comparator;
    }

    public boolean hasComparator()
    {
        return this.comparator != null;
    }
}
