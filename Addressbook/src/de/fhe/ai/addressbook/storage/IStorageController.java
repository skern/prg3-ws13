package de.fhe.ai.addressbook.storage;

import de.fhe.ai.addressbook.model.Addressbook;

import java.io.FileNotFoundException;

/**
 * Interface defining methods required from any class that offers
 * load and save functionality for an addressbook.
 *
 * The interface just defines two methods - one for saving an
 * addressbook and a second one for loading an addressbook.
 */
public interface IStorageController
{
    void saveAddressbook( Addressbook addressbook );

    Addressbook loadAddressbook() throws FileNotFoundException;
}
