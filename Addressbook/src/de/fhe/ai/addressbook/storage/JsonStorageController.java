package de.fhe.ai.addressbook.storage;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fhe.ai.addressbook.control.ExceptionHandler;
import de.fhe.ai.addressbook.model.Addressbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Implementation of the <code>IStorageController</code> interface
 * which uses the Jackson JSON Library to save addressbook instances
 * as JSON strings to the file system.
 */
public class JsonStorageController implements IStorageController
{
    private static final String HOME_DIR = System.getProperty( "user.dir" );
    private static final String FILE_NAME = "data.json";
    private static final String FULL_PATH = HOME_DIR + File.separator + FILE_NAME;


    @Override
    public void saveAddressbook( Addressbook addressbook )
    {
        try
        {
            File jsonFile = new File( FULL_PATH );

            if( !jsonFile.exists() ) jsonFile.createNewFile();

            ObjectMapper mapper = new ObjectMapper();
            mapper.writerWithDefaultPrettyPrinter().writeValue( jsonFile, addressbook );
        }
        catch ( IOException e )
        {
            ExceptionHandler.handle( e, this );
        }
    }

    @Override
    public Addressbook loadAddressbook() throws FileNotFoundException
    {
        Addressbook ab = null;

        File jsonFile = new File( FULL_PATH );

        if( !jsonFile.exists() ) throw new FileNotFoundException( "Addressbook JSON File not found" );

        try
        {
            ObjectMapper mapper = new ObjectMapper();

            ab = mapper.readValue( jsonFile, Addressbook.class );
        }
        catch ( IOException e )
        {
            ExceptionHandler.handle( e, this, "Error parsing JSON from file'" + FULL_PATH + "'" );
        }

        return ab;
    }
}
