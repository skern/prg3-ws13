package de.fhe.ai.addressbook.storage;

import de.fhe.ai.addressbook.control.ExceptionHandler;
import de.fhe.ai.addressbook.model.Addressbook;

import java.io.*;

/**
 * Implementation of the <code>IStorageController</code> interface
 * which uses the built-in Java ObjectOutputStreams to save addressbook
 * instances as binary data to the file system.
 */
public class ObjectStorageController implements IStorageController
{
    private static final String HOME_DIR = System.getProperty( "user.dir" );
    private static final String FILE_NAME = "data.jab";
    private static final String FULL_PATH = HOME_DIR + File.separator + FILE_NAME;

    @Override
    public void saveAddressbook( Addressbook addressbook )
    {
        ObjectOutputStream oos = null;

        try
        {
            File dataFile = new File( FULL_PATH );

            if( !dataFile.exists() )
            {
                dataFile.createNewFile();
            }

            FileOutputStream fos = new FileOutputStream( dataFile );
            BufferedOutputStream bos = new BufferedOutputStream( fos );
            oos = new ObjectOutputStream( bos );

            oos.writeObject( addressbook );

            oos.flush();
        }
        catch ( IOException e )
        {
            ExceptionHandler.handle( e, this );
        }
        finally
        {
            try
            {
                if( oos != null )
                    oos.close();
            }
            catch ( IOException e )
            {
                ExceptionHandler.handle( e, this );
            }
        }
    }

    @Override
    public Addressbook loadAddressbook() throws FileNotFoundException
    {
        Addressbook addressbook = null;
        File dataFile = new File( FULL_PATH );

        if( !dataFile.exists() )
            throw new FileNotFoundException( "Data File does not exist" );

        ObjectInputStream ois = null;

        try
        {
            FileInputStream fis = new FileInputStream( dataFile );
            BufferedInputStream bis = new BufferedInputStream( fis );
            ois = new ObjectInputStream( bis );

            addressbook = (Addressbook)ois.readObject();
        }
        catch ( IOException e )
        {
            ExceptionHandler.handle( e, this );
        }
        catch ( ClassNotFoundException cnfe )
        {
            ExceptionHandler.handle( cnfe, this, "Addressbook class could not be found" );
        }
        finally
        {
            try
            {
                if( ois != null )
                    ois.close();
            }
            catch ( IOException e )
            {
                ExceptionHandler.handle( e, this, "Could not close ObjectInputStream" );
            }
        }

        return addressbook;
    }


}
