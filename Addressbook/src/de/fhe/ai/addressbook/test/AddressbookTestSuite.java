package de.fhe.ai.addressbook.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith( Suite.class )
@Suite.SuiteClasses({
        TestAddressbook.class,
        TestPerson.class
})

public class AddressbookTestSuite {}
