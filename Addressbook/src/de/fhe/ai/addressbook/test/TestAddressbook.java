package de.fhe.ai.addressbook.test;

import de.fhe.ai.addressbook.model.Addressbook;
import de.fhe.ai.addressbook.model.Person;
import de.fhe.ai.addressbook.model.SortCriteriaEnum;
import org.junit.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: steffenkern
 * Date: 12.12.13
 * Time: 12:37
 * To change this template use File | Settings | File Templates.
 */
public class TestAddressbook
{
    private Addressbook ab;

    @Before
    public void setUp() throws Exception
    {
        ab = new Addressbook();
    }

    @After
    public void tearDown() throws Exception
    {
        // Could be used to clean up after each test method
    }


    @Test
    public void testAddPerson() throws Exception
    {
        ab.addPerson( getFirstPerson() );

        Assert.assertEquals( ab.getPersons().size(), 1 );
        Assert.assertEquals( ab.getPersons().get( 0 ).getLastName(), TestConstants.FIRST_PERSON_LASTNAME );
        Assert.assertEquals( ab.getPersons().get( 0 ).getFirstName(), TestConstants.FIRST_PERSON_FIRSTNAME );
        Assert.assertEquals( ab.getPersons().get( 0 ).getMail(), TestConstants.FIRST_PERSON_MAIL );
    }

    @Test
    public void testGetPersons() throws Exception
    {
        ab.addPerson( getFirstPerson() );
        ab.addPerson( getSecondPerson() );

        List<Person> persons = ab.getPersons();

        Assert.assertEquals( persons.size(), 2 );
        Assert.assertEquals( persons.get( 1 ).getLastName(), TestConstants.SECOND_PERSON_LASTNAME );

        Assert.assertTrue( persons.get( 0 ).equals( getFirstPerson() ) );

    }

    @Test
    public void testSortPersonList() throws Exception
    {
        ab.addPerson( getFirstPerson() );
        ab.addPerson( getSecondPerson() );

        List<Person> sortedPersons = ab.sortPersonListWithEnhancedEnum( SortCriteriaEnum.LASTNAME_ASC );

        Assert.assertEquals( sortedPersons.get( 0 ).getFirstName(), TestConstants.SECOND_PERSON_FIRSTNAME );
    }

    @Test
    public void testSortPersonListWithEnhancedEnum() throws Exception
    {
        ab.addPerson( getFirstPerson() );
        ab.addPerson( getSecondPerson() );

        List<Person> sortedPersons = ab.sortPersonListWithEnhancedEnum( SortCriteriaEnum.LASTNAME_DESC );

        Assert.assertEquals( sortedPersons.get( 0 ).getFirstName(), TestConstants.FIRST_PERSON_FIRSTNAME );
    }

    /*
        Internal Helper Methods
     */
    private Person getFirstPerson()
    {
        return getPerson( TestConstants.FIRST_PERSON_FIRSTNAME,
                TestConstants.FIRST_PERSON_LASTNAME, TestConstants.FIRST_PERSON_MAIL );
    }

    private Person getSecondPerson()
    {
        return getPerson( TestConstants.SECOND_PERSON_FIRSTNAME,
                TestConstants.SECOND_PERSON_LASTNAME, TestConstants.SECOND_PERSON_MAIL );
    }

    private Person getPerson( String firstName, String lastName, String mail )
    {
        return new Person( firstName, lastName, mail );
    }
}
