package de.fhe.ai.addressbook.test;

/**
 * Created with IntelliJ IDEA.
 * User: steffenkern
 * Date: 12.12.13
 * Time: 16:20
 * To change this template use File | Settings | File Templates.
 */
public class TestConstants
{
    protected static final String FIRST_PERSON_LASTNAME = "Mustermann";
    protected static final String FIRST_PERSON_FIRSTNAME = "Max";
    protected static final String FIRST_PERSON_MAIL = "m.m@muster.com";

    protected static final String SECOND_PERSON_LASTNAME = "Alm";
    protected static final String SECOND_PERSON_FIRSTNAME = "Heidi";
    protected static final String SECOND_PERSON_MAIL = "heidi@alm.ch";

}
