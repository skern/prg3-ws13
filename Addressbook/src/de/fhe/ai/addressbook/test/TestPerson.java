package de.fhe.ai.addressbook.test;

import de.fhe.ai.addressbook.model.Person;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestPerson
{
    private Person person;

    @Before
    public void setUp() throws Exception
    {
        person = new Person( TestConstants.FIRST_PERSON_FIRSTNAME,
                TestConstants.FIRST_PERSON_LASTNAME, TestConstants.FIRST_PERSON_MAIL);
    }

    @Test
    public void testEquals() throws Exception
    {

        Assert.assertNotNull( "Not Null - person should not be null", person );

        Assert.assertEquals( "person should be equal to itself", person, person );

        Assert.assertEquals( "person should be equal to a person instance with the same values",
                             person,
                             new Person( TestConstants.FIRST_PERSON_FIRSTNAME,
                                TestConstants.FIRST_PERSON_LASTNAME, TestConstants.FIRST_PERSON_MAIL )  );

        Assert.assertFalse( "Two different persons should never be equal",
                person.equals( new Person( TestConstants.SECOND_PERSON_FIRSTNAME,
                        TestConstants.SECOND_PERSON_LASTNAME, TestConstants.SECOND_PERSON_MAIL ) ) );

        Assert.assertSame( "It should true that person == person ", person, person );

        Assert.assertNotSame( "It should not be true that person == other_person",
                person,
                new Person( TestConstants.FIRST_PERSON_FIRSTNAME,
                        TestConstants.FIRST_PERSON_LASTNAME, TestConstants.FIRST_PERSON_MAIL ) );
    }
}
