import java.io.File;

public class FileSystemDirectoryPrinter
{
	public static void main(String[] args)
	{
		File userDir = new File( System.getProperty( "user.home" ) );

		File[] dirContent = userDir.listFiles();

		for( File entry : dirContent )
		{
			if( entry.isDirectory() )
				System.out.print("(D)\t");
			else
				System.out.print("(F)\t");

			System.out.println(entry.getName());
		}
	}
}
