import java.io.*;

public class FileSystemHierarchyFileWriter
{
	private static int totalFileCount;

	public static void printDirectory( File dir, int level, PrintStream stream )
	{
		// Prefix für jede Zeile abhängig von der Tiefe in
		// der Hierarchie
		StringBuilder levelPrefixBuilder = new StringBuilder( "" );

		for (int i = 0; i < level; i++ )
		{
			levelPrefixBuilder.append( "-" );
		}

		String levelPrefix = levelPrefixBuilder.toString();

		File[] dirContent = dir.listFiles();

		for( File entry : dirContent )
		{
			totalFileCount++;

			stream.print( levelPrefix + " ");

			if( entry.isDirectory() )
				stream.print("(D)\t");
			else
				stream.print("(F)\t");

			stream.println(entry.getName());

			if( entry.isDirectory() ) printDirectory( entry, level + 1, stream );
		}
	}

	public static void main(String[] args)
	{
		File userDir = new File( System.getProperty( "user.home" ) );
		PrintStream fileWriter = null;

		try
		{
			File output = new File( "filesystem.txt" );
			output.createNewFile();
			fileWriter = new PrintStream( output );
			printDirectory( userDir, 0, fileWriter );
		}
		catch (IOException e) {
			System.out.println( "Error while trying to write to file" );
		}
		finally {
			if( fileWriter != null ) {
				fileWriter.flush();
				fileWriter.close();
			}
		}
	}
}
