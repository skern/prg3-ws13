import java.io.File;

public class FileSystemHierarchyPrinter
{
	public static void printDirectory( File dir, int level )
	{
		StringBuilder levelPrefixBuilder = new StringBuilder( "" );
		for (int i = 0; i < level; i++ )
		{
			levelPrefixBuilder.append( "-" );
		}
		levelPrefixBuilder.append( " " );
		String levelPrefix = levelPrefixBuilder.toString();

		File[] dirContent = dir.listFiles();

		for( File entry : dirContent )
		{
			System.out.print( levelPrefix );

			if( entry.isDirectory() )
			{
				System.out.print("(D) ");
			}
			else
			{
				System.out.print("(F) ");
			}

			System.out.println(entry.getName());

			if( entry.isDirectory() ) printDirectory( entry, level + 1 );
		}
	}

	public static void main(String[] args)
	{
		File userDir = new File( System.getProperty( "user.home" ) );
		printDirectory( userDir, 0 );
	}

}
