package de.fhe.ai.carmanufacturer.v1;

public class ManufacturerTest
{
    public static void main(String[] args)
    {
        /*
            Create a Manufacturer instance for Multicar.
            http://www.multicar.de/

            Revenues are fictional values...
         */

        // Create Manufacturer by calling the Constructor
        Manufacturer m = new Manufacturer( "Multicar", "Waltershausen", 1250, 4 );

        // Add some revenues to our Manufacturer Instance m
        m.addRevenue( 20.0 );
        m.addRevenue( 22.0 );
        m.addRevenue( 21.5 );
        m.addRevenue( 24.0 );
        m.addRevenue( 23.8 );

        // Also add some cars
        m.addCar( "FUMO" );
        m.addCar( "M27" );
        m.addCar( "M31" );
        m.addCar( "TREMO" );

        // Print the Manufacturer instance we just created and configured
        // This will result in a call to the toString() method
        System.out.println( m );


        /*
            Testing the parameter-less constructor
         */
        Manufacturer m2 = new Manufacturer();
        System.out.println( m2 );

    }




}
