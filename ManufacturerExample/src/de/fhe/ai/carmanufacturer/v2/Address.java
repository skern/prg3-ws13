package de.fhe.ai.carmanufacturer.v2;

/**
 * Created with IntelliJ IDEA.
 * User: kern
 * Date: 12.07.13
 * Time: 15:43
 */

/**
 * A class to represent an address.
 * <p/>
 * Good example for so-called "Value Objects" - these are objects that contain
 * only data without any or much functionality or logic. Their primary use is
 * to encapsulate data that belongs together (similar to a struct in C), to
 * provide (controlled) access to that data as well as to allow the transfer/passing
 * of that data around the program.
 */
public class Address
{
    private String street;
    private int houseNumber;
    private String city;
    private String zipCode;
    private String country;

    /**
     * Main Constructor to create an <code>Address</code> instance.
     * <p/>
     * Initializes all relevant member variables of an <code>Address</code> object.
     *
     * @param street the street
     * @param houseNumber the house number
     * @param city the city
     * @param zipCode the postal code
     * @param country the country
     */
    public Address( String street, int houseNumber, String city, String zipCode, String country )
    {
        this.street = street;
        this.houseNumber = houseNumber;
        this.city = city;
        this.zipCode = zipCode;
        this.country = country;
    }

    /**
     * Non-parameter constructor to initialize an "empty" <code>Address</code> instance.
     * <p/>
     * Uses some more or less meaningful values to call the main constructor.
     */
    public Address()
    {
        this( "", -1, "", "", "" );
    }

    /*
        Overridden from base class
     */

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();

        sb.append( this.getStreet() ).append( " " ).append( this.getHouseNumber() ).append( "\n" );
        sb.append( this.getZipCode() ).append( " " ).append( this.getCity() ).append( "\n" );
        sb.append( this.getCountry() );

        return sb.toString();
    }

    /*
        Getter & Setter
     */

    public String getStreet()
    {
        return street;
    }

    public void setStreet( String street )
    {
        this.street = street;
    }

    public int getHouseNumber()
    {
        return houseNumber;
    }

    public void setHouseNumber( int houseNumber )
    {
        this.houseNumber = houseNumber;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity( String city )
    {
        this.city = city;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setZipCode( String zipCode )
    {
        this.zipCode = zipCode;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry( String country )
    {
        this.country = country;
    }
}
