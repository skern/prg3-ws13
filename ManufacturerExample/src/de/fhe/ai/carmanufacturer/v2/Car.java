package de.fhe.ai.carmanufacturer.v2;

/**
 * Created with IntelliJ IDEA.
 * User: kern
 * Date: 11.07.13
 * Time: 13:28
 */

/**
 * This class models a single car with all of its attributes.
 */
public class Car
{
    private String name;
    private int numberOfSeats;
    private double loadCapacity;
    private int horsePower;

    /**
     * Main Constructor for cars.
     *
     * @param name  Name of the car
     * @param numberOfSeats Number of passenger seats in car
     * @param loadCapacity Maximum cargo the car can carry
     * @param horsePower The cars power
     */
    public Car( String name, int numberOfSeats, double loadCapacity, int horsePower )
    {
        this.name = name;
        this.numberOfSeats = numberOfSeats;
        this.loadCapacity = loadCapacity;
        this.horsePower = horsePower;
    }

    public Car( String name )
    {
        this( name, 0, 0.0, 0 );
    }

    public Car()
    {
        this( "no name" );
    }

    /*
        Overridden from base class Object
     */

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();

        sb.append( this.getName() ).append( "\n" );
        sb.append( "\tSeats:         " ).append( this.getNumberOfSeats() ).append( "\n" );
        sb.append( "\tLoad Capacity: " ).append( this.getLoadCapacity() ).append( "\n" );
        sb.append( "\tHorse Power:   " ).append( this.horsePower );

        return sb.toString();
    }

    /*
        Getter and Setter to access member
     */

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public int getNumberOfSeats()
    {
        return numberOfSeats;
    }

    public void setNumberOfSeats( int numberOfSeats )
    {
        this.numberOfSeats = numberOfSeats;
    }

    public double getLoadCapacity()
    {
        return loadCapacity;
    }

    public void setLoadCapacity( double loadCapacity )
    {
        this.loadCapacity = loadCapacity;
    }

    public int getHorsePower()
    {
        return horsePower;
    }

    public void setHorsePower( int horsePower )
    {
        this.horsePower = horsePower;
    }
}
