package de.fhe.ai.carmanufacturer.v2;

/**
 * This class represents a car manufacturer.
 */
public class Manufacturer
{
    // Some constants
    private static final int MAX_NUMBER_OF_REVENUE_YEARS = 10;

    // Some members
    private String name;
    private Address address;
    private int numberOfEmployees;

    // A Car array containing all cars
    private Car[] cars;
    // Track the number of cars
    private int currentNumberOfCars = 0;

    // Contains revenues of the last 10 years
    // in millions
    private Revenue[] revenues;

    /**
     * Default/Master Constructor
     * <p/>
     * Creates a new Manufacturer instance
     *
     * @param name              name of the manufacturer
     * @param address           address of the manufacturer
     * @param numberOfEmployees the total number of employees
     * @param numberOfCars      number of cars - determines the size of the cars array
     */
    public Manufacturer( String name, Address address, int numberOfEmployees, int numberOfCars )
    {
        this.name = name;
        this.address = address;
        this.numberOfEmployees = numberOfEmployees;
        this.cars = new Car[numberOfCars];
        this.revenues = new Revenue[MAX_NUMBER_OF_REVENUE_YEARS];
    }

    /**
     * This Constructor has no parameters - it just creates
     * a new Manufacturer with some more or less meaningful default
     * values.
     * <p/>
     * This constructor calls the other constructor defined in this
     * class by using the <code>this</code> keyword. This technique
     * should always be used if a class defines more than one constructor
     * (remember the "Don't repeat yourself" (DRY) principle!).
     * <p/>
     * The following steps should be taken:
     * <p/>
     * 1. Define one "Master" constructor that does all the relevant
     * initialization tasks. This is usually the constructor with the most
     * parameters.
     * <p/>
     * 2. Define the other constructors. These should all use the Master
     * constructor and provide meaningful default values for all parameters
     * that are not part of their own parameter sets.
     * <p/>
     * In this class, our parameter-less constructor provides meaningful
     * values for <code>name</code>, <code>address</code>,
     * <code>numberOfEmployees</code> and <code>numberOfCars</code>.
     */
    public Manufacturer()
    {
        this( "Manufacturer Name", new Address(), 0, 5 );
    }


    /****************************************
        Some general methods
     ****************************************/

    /**
     * Based on the number of employees, this method returns a
     * String which describes the company size.
     *
     * @return a String describing the size of the company
     */
    public String getManufacturerSize()
    {
        if( this.getNumberOfEmployees() <= 0 )
        {
            return "This company is dead.";
        }
        else if( this.getNumberOfEmployees() == 1 )
        {
            return "One man army.";
        }
        else if( this.getNumberOfEmployees() < 250 )
        {
            return "Small Company.";
        }
        else
        {
            return "Big player";
        }
    }

    /****************************************
        Some methods for revenue handling
     ****************************************/

    /**
     * Adds a new revenue to the revenues array
     * Revenues at smaller indices are younger/newer
     * Thus, before adding the new revenues, we move all old
     * revenues one place backwards. The oldest revenue is
     * dropped during this procedure.
     *
     * @param newRevenue the new revenues which should be added
     */
    public void addRevenue( Revenue newRevenue )
    {
        for ( int i = MAX_NUMBER_OF_REVENUE_YEARS - 1; i > 0 ; i-- )
        {
            this.revenues[i] = this.revenues[i-1];
        }

        revenues[0] = newRevenue;
    }

    /**
     * Overridden method to add a revenue
     * <p/>
     * It first creates a new <code>Revenue</code> instance be using the provided parameter values.
     * Afterwards, the other <code>addRevenue()</code> method is called to actually
     * add the new <code>Revenue</code> instance to the manufacturer.
     *
     * @param revenue the revenue amount
     * @param year the revenue year
     */
    public void addRevenue( double revenue, int year )
    {
        this.addRevenue( new Revenue( revenue, year ) );
    }

    /**
     * Sums up all the revenues
     *
     * @return the total revenue amount
     */
    public double getTotalRevenue()
    {
        double revenue = 0.0;

        for ( int i = 0; i < getNumberOfRevenueYears(); i++ )
        {
            revenue += this.revenues[i].getAmount();
        }

        return revenue;
    }

    /**
     * Determines the average revenue
     *
     * @return the average revenue
     */
    public double getMeanRevenue()
    {
        return this.getTotalRevenue() / getNumberOfRevenueYears();
    }

    /**
     * Creates a String that contains a list of all revenues.
     *
     * @return the revenues as String
     */
    public String allRevenuesToString()
    {
        StringBuffer buffer = new StringBuffer();

        for ( int i = 0; i < getNumberOfRevenueYears(); i++ )
        {
            buffer.append( "- ").append( this.revenues[i] ).append( "\n" );
        }
        buffer.append( "\n" );

        return buffer.toString();
    }

    /****************************************
        Methods for handling cars
     ****************************************/

    /**
     * This method adds a new car to the list of cars
     * If the maximum number of cars for this company
     * is reached, the new car is not added and an
     * error message is written to the console
     *
     * @param car the new car to be added
     */
    public void addCar( Car car )
    {
        // Still space left for a new car?
        if( currentNumberOfCars < this.cars.length )
        {
            this.cars[currentNumberOfCars] = car;
            currentNumberOfCars++;
        }
        else
        {
            System.out.println( "No space for a new car!" );
        }
    }

    /**
     * Overloaded method to add a car to a <code>Manufacturer</code> instance.
     * <p/>
     * Using the values passed to this method, a new <code>Car</code> instance
     * is created an added to the <code>Manufacturer</code> instance by calling
     * the other <code>addCar()</code> method.
     *
     * @param name the name of the car
     * @param numberOfSeats the number of available seats
     * @param loadCapacity the capacaty of cargo that can be transported by the car
     * @param horsePower the cars horse powers
     */
    public void addCar( String name, int numberOfSeats, double loadCapacity, int horsePower )
    {
        this.addCar( new Car( name, numberOfSeats, loadCapacity, horsePower) );
    }


    /**
     * Returns a String containing a listing of all cars
     * <p/>
     * Uses (implicit) the <code>toString()</code> method of the <code>Car</code>
     * class.
     *
     * @return all cars as list in a String
     */
    public String allCarsToString()
    {
        StringBuffer buffer = new StringBuffer();

        for( Car car : this.cars )
        {
            buffer.append( "- " ).append( car ).append( "\n" );
        }
        buffer.append( "\n" );

        return buffer.toString();
    }

    /****************************************
        Methods overridden from Base Class
     ****************************************/

    /**
     * A method overridden from the base class <code>Object</code>
     * to produce a nice output for a Manufacturer instance
     * <p/>
     * In Java, the <code>toString()</code> method is called automatically
     * when an object is written to an output stream like System.out. Thus,
     * overriding this method is always a good idea.
     *
     * @return a String representation of a manufacturer
     */
    @Override
    public String toString()
    {
        StringBuffer buffer = new StringBuffer();

        buffer.append( "** Company Information **\n" );
        buffer.append( this.getName()).append( " \nAddress:\n" ).append( this.getAddress() ).append( "\n" );
        buffer.append( "Company Size: " ).append( this.getManufacturerSize() ).append( "\n\n" );

        buffer.append( "** Number of Employees **\n" );
        buffer.append( this.getNumberOfEmployees() ).append( " Employees\n\n");

        buffer.append( "** Revenue **\n");
        buffer.append( allRevenuesToString() );
        buffer.append( "Average Revenue: " ).append(  this.getMeanRevenue() ).append( "\n" );
        buffer.append( "Total Revenue: " ).append( this.getTotalRevenue() ).append( "\n\n" );

        buffer.append( "** Cars **\n" );
        buffer.append( this.allCarsToString() ).append( "\n\n" );

        return buffer.toString();
    }

    /****************************************
        Private Helper Methods
     ****************************************/

    /**
     * Determines the number of revenue entries
     * @return the number of years (always <= MAX_NUMBER_OF_REVENUE_YEARS)
     */
    private int getNumberOfRevenueYears()
    {
        int numberOfYears = 0;
        for( Revenue revenue : this.revenues )
        {
            if( revenue != null )
            {
                numberOfYears++;
            }
        }

        return numberOfYears;
    }

    /****************************************
        Getter and Setter
     ****************************************/

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public Address getAddress()
    {
        return address;
    }

    public void setAddress( Address address )
    {
        this.address = address;
    }

    public int getNumberOfEmployees()
    {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees( int numberOfEmployees )
    {
        this.numberOfEmployees = numberOfEmployees;
    }

    public Car[] getCars()
    {
        return cars;
    }

    public Revenue[] getRevenues()
    {
        return revenues;
    }
}
