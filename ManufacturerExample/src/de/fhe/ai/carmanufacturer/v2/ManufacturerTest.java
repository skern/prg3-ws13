package de.fhe.ai.carmanufacturer.v2;

public class ManufacturerTest
{
    public static void main(String[] args)
    {
        /*
            Create a Manufacturer instance for Multicar.
            http://www.multicar.de/

            Revenues are fictional values...
         */

        // Create a new Address for the Manufacturer
        Address address = new Address( "Industriestrasse", 3, "Waltershausen", "99880", "Deutschland" );

        // Create Manufacturer by calling the Constructor
        Manufacturer m = new Manufacturer( "Multicar", address, 1250, 4 );

        // Add some revenues to our Manufacturer Instance m
        m.addRevenue( 20.0, 2008 );
        m.addRevenue( 22.0, 2009 );
        m.addRevenue( 21.5, 2010 );
        m.addRevenue( 24.0, 2011 );
        m.addRevenue( 23.8, 2012 );
        m.addRevenue( new Revenue( 23.6, 2013 ) );

        // Also add some cars
        m.addCar( "FUMO", 2, 2700.0, 145  );
        m.addCar( "M27", 2, 3000.0, 102  );
        m.addCar( "M31", 2, 3110.0, 145  );
        m.addCar( new Car( "TREMO", 2, 2550.0, 102 ) );

        // Print the Manufacturer instance we just created and configured
        // This will result in a call to the toString() method
        System.out.println( m );
    }
}
