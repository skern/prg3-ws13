package de.fhe.ai.carmanufacturer.v2;

/**
 * Created with IntelliJ IDEA.
 * User: kern
 * Date: 12.07.13
 * Time: 12:02
 */
public class Revenue
{
    private double amount;
    private int year;

    /**
     * Main Constructor to create a <code>Revenue</code> instance.
     *
     * @param amount the amount of revenue
     * @param year revenue year
     */
    public Revenue( double amount, int year )
    {
        this.amount = amount;
        this.year = year;
    }

    /**
     * A second constructor without parameters. Can be used to create an
     * "empty" <code>Revenue</code> instance.
     * <p/>
     * Calls the main constructor with <code>0.0</code> for revenue and
     * <code>0</code> for year parameter.
     */
    public Revenue()
    {
        this( 0.0, 0 );
    }

    /*
        Overridden from base class
     */

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();

        sb.append( "Year: " ).append( this.getYear() );
        sb.append( " - Amount: " ).append( this.getAmount() );

        return sb.toString();
    }
    /*
        Getter and Setter
     */

    public double getAmount()
    {
        return amount;
    }

    public void setAmount( double amount )
    {
        this.amount = amount;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear( int year )
    {
        this.year = year;
    }
}
