package de.fhe.ai.prg3.uebung01;

/**
 * A class that allows for playing the "FitzQuark" game.
 */
public class Excercise01FitzQuark
{

    public static void main( String[] args )
    {
        for( int i = 1; i <=100; i++ )
        {
            if( i % 5 == 0 && i % 7 == 0 )
            {
                System.out.print( "FitzQuark, " );
            }
            else if( i % 5 == 0 )
            {
                System.out.print( "Fitz, " );
            }
            else if( i % 7 == 0 )
            {
                System.out.print( "Quark, " );
            }
            else
            {
                System.out.print( i + ", " );
            }
        }
    }
}
