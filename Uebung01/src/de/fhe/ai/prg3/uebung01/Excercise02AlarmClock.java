package de.fhe.ai.prg3.uebung01;

/**
 * Alarm Clock class.
 *
 * It simply allows for setting an alarm time as well as a boolean value
 * to indicate if the alarm clock is active or not.
 *
 * The method to set the alarm time will check for meaningful values.
 */
public class Excercise02AlarmClock
{
    private boolean isActive;
    private int alarmHours;
    private int alarmMinutes;

    public Excercise02AlarmClock()
    {
        this.isActive = false;
        this.alarmHours = 0;
        this.alarmMinutes = 0;
    }

    public void setAlarmTime( int hours, int minutes )
    {
        // Check correct value for hour
        if ( hours < 0 || hours > 23 )
        {
            System.out.println( "Hour Error" );
            return;
        }

        this.alarmHours = hours;

        // Check correct value for minutes
        if( minutes < 0 || minutes > 59  )
        {
            System.out.println( "Minutes Error" );
            return;
        }

        this.alarmMinutes = minutes;
    }

    public String getAlarmTime()
    {
        return this.alarmHours + ":" + this.alarmMinutes;
    }

    public boolean isActive()
    {
        return isActive;
    }

    public void setActive( boolean active )
    {
        isActive = active;
    }

    /**
     * Test Method for our Alarm Clock
     *
     * @param args Command line arguments
     */
    public static void main( String[] args )
    {
        // Create new Alarm Clock
        Excercise02AlarmClock alarmClock = new Excercise02AlarmClock();

        // Provoke Hour Error
        alarmClock.setAlarmTime( 43, 12 );

        // Provoke Minutes Error
        alarmClock.setAlarmTime( 12, 98 );

        // Set valid alarm time
        alarmClock.setAlarmTime( 1, 34 );

        // Check Alarm Time
        System.out.println( alarmClock.getAlarmTime() );

    }
}
