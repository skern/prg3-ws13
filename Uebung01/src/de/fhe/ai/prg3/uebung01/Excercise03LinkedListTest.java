package de.fhe.ai.prg3.uebung01;

/**
 * Class used to test the LinkedList as well as the ListNode classes.
 */
public class Excercise03LinkedListTest
{
    public static void main( String[] args )
    {
        LinkedList myList = new LinkedList( 6 );
        myList.addNode( 3 );
        myList.addNode( 45 );
        myList.addNode( -4 );

        myList.printAllNodes();
    }
}
