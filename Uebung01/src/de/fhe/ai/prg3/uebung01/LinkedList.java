package de.fhe.ai.prg3.uebung01;

/**
 * This class defines a singly linked list.
 *
 * It holds a reference to the first node inside the list (head)
 * and offers some methods to add element as well as print all
 * list nodes.
 */
public class LinkedList
{
    private ListNode headNode;

    /**
     * First constrictor.
     * Has a single int value as parameter, which is used to create the new list node
     * It afterwards uses the second constructor defined in this class to set
     * this newly created node as head node.
     * @param headValue value of the node which should become the list head
     */
    public LinkedList( int headValue )
    {
        this( new ListNode( headValue ) );
    }

    /**
     * Second constructor.
     * Takes a list node as argument to use it as the lists head element.
     * @param headNode the lists head node
     */
    public LinkedList( ListNode headNode )
    {
        this.headNode = headNode;
    }

    /**
     * Returns the head node of the list.
     *
     * @return - the lists head node
     */
    public ListNode getHeadNode()
    {
        return headNode;
    }

    /**
     * Adds the given node at the end of the list.
     * @param newNode the node to be added
     */
    public void addNode( ListNode newNode )
    {
        ListNode currentNode = this.getHeadNode();

        while( currentNode.getNextNode() != null )
        {
            currentNode = currentNode.getNextNode();
        }

        currentNode.setNextNode( newNode );
    }

    /**
     * Overridden method.
     *
     * Uses the given int value to create a new list node object
     * which is afterwards used for calling the other version
     * of this method to add the newly created node to the list.
     *
     * @param newValue the value for the new node which should be added to the list
     */
    public void addNode( int newValue )
    {
        this.addNode( new ListNode( newValue ) );
    }


    /**
     * This method iterates through the list and prints all values stored by the list.
     */
    public void printAllNodes()
    {
        ListNode currentNode = this.getHeadNode();

        while( currentNode != null )
        {
            System.out.println( currentNode.getNodeValue() );
            currentNode = currentNode.getNextNode();
        }
    }
}
