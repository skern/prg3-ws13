package de.fhe.ai.prg3.uebung01;

/**
 * A simple class to model nodes in a linked list.
 *
 * It stores an int value as well as a reference to the next
 * node in the list.
 *
 * Moreover, it provides Getter and Setter methods to access these two members.
 */
public class ListNode
{
    private ListNode nextNode;
    private int nodeValue;

    public ListNode( int nodeValue )
    {
        this.nodeValue = nodeValue;
        this.nextNode = null;
    }

    public ListNode getNextNode()
    {
        return nextNode;
    }

    public void setNextNode( ListNode nextNode )
    {
        this.nextNode = nextNode;
    }

    public int getNodeValue()
    {
        return nodeValue;
    }

    public void setNodeValue( int nodeValue )
    {
        this.nodeValue = nodeValue;
    }
}
